# índex de sobirania tecnològica dels operadors de telecomunicacions

per descarregar l'estat actual només de la pàgina web en qüestió i plasmar-lo aquí farem servir la següent comanda [font](https://superuser.com/questions/55040/save-a-single-web-page-with-background-images-with-wget/136335#136335)

    wget -E -H -k -K -p https://fundacio.guifi.net/page/sobirania-tecnologica

per veure la web en local executarem un servidor web al directori arrel d'aquest repositori on ens trobem

    python -m SimpleHTTPServer

i llavors podrem visitar la pàgina al següent enllaç: http://localhost:8000/fundacio.guifi.net/page/sobirania-tecnologica.html

nota: el servidor ens avisa que falten alguns fitxers però la visualització és correcta i s'entén que prou completa per modificar i control·lar l'estat de la pàgina

s'estan fent proves en una taula markdown i renderitzat d'aquesta en HTML amb estil CSS [font](https://gist.github.com/killercup/5917178)

    pandoc -f gfm --metadata pagetitle="taula" --css pandoc.css -s taula.md -o taula.html

instal·lar compass

    sudo gem install compass

generar CSS (energylabel.css)

    compass compile

# crèdits

- fundació guifi va començar la web i idea https://fundacio.guifi.net/page/sobirania-tecnologica
- @pedrolab va fer reformulació del concepte amb filosofia forja-git i implementació
- energy labels in css inspired by [Bastien Heynderickx (hachbé)](https://codepen.io/hachbe/pen/EaPqjq)
