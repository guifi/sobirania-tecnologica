#!/bin/bash

compass compile
cat energylabel.css pandoc.css > all.css
pandoc -f gfm --metadata pagetitle="taula" --css all.css -s taula.md -o taula.html
