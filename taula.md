| Operadora | Xarxa de Comuns | Economia inclusiva | Atenció a l'usuari/a en català** | Seu social local | Economia social i solidària amb funcionament participatiu | Valoració global |
| - | - | - | - | - | - | - |
| [ADAMO](https://www.adamo.es) | ❌ | ❌ | ✅ | ❌ | ❌ | <span class="classD">D</span> |
| [ALT URGELL FIBRA](https://www.alturgellfibra.cat) | ✅ | ✅ | ✅ | ✅ | ❌ | <span class="classA">A</span> |
| [AMENA](https://amena.com) | ❌ | ❌ | ❌ | ❌ | ❌ | <span class="class0">0</span> |
| [CAPA8](https://capa8.net) | ✅ | ✅ | ✅ | ✅ | ❌ | <span class="classA">A</span> |
| CASTELLET | ✅ | ✅ | ✅ | ✅ | ❌ | <span class="classA">A</span> |
| [CITTEC](http://cittec.cat) - [SOOM FIBRA](https://soomfibra.cat) | ✅ | ✅ | ✅ | ✅ | ❌ | <span class="classA">A</span> |
| [COMMU](http://commu.cat) | ❌ | ✅ | ✅ | ✅ | ❌ | <span class="classB">B</span> |
| [DELINTERNET](https://delinternet.com) | ✅ | ✅ | ✅ | ✅ | ❌ | <span class="classA">A</span> |
| [EBRECOM](http://ebrecom.com) | ✅ | ✅ | ✅ | ✅ | ❌ | <span class="classA">A</span> |
| [EMAGINA](https://www.emagina.cat) | ✅ | ✅ | ✅ | ✅ | ❌ | <span class="classA">A</span> |
| [EMPORDÀ WIFI](https://empordawifi.com) | ✅ | ✅ | ✅ | ✅ | ❌ | <span class="classA">A</span> |
| [E.TELECOM](http://etelecom.es) | ✅ | ✅ | ✅ | ✅ | ❌ | <span class="classA">A</span> |
| [EXO](https://exo.cat) | ✅ | ✅ | ✅ | ✅ | ✅ | <span class="classAplus">A+</span> |
| [EUSKALTEL](https://www.euskaltel.com) | ❌ | ❌ | ❌ | ❌ | ❌ | <span class="class0">0</span> |
| [FIBRA ÒPTICA CENTELLES](http://www.focentelles.net) | ❌ | ✅ | ✅ | ✅ | ❌ | <span class="classB">B</span> |
| [GIRONA FIBRA](https://www.gironafibra.cat) | ✅ | ✅ | ✅ | ✅ | ❌ | <span class="classA">A</span> |
| [GOTES.ORG](https://www.gotes.org) | ✅ | ✅ | ✅ | ✅ | ❌ | <span class="classA">A</span> |
| [GOUFONE](https://goufone.com) | ✅ | ✅ | ✅ | ✅ | ❌ | <span class="classA">A</span> |
| [GUIFIBAGES](https://guifibages.cat) | ✅ | ✅ | ✅ | ✅ | ✅ | <span class="classAplus">A+</span> |
| [IGUANA](https://iguana.cat) | ✅ | ✅ | ✅ | ✅ | ❌ | <span class="classA">A</span> |
| [INDALECCIUS](http://indaleccius.com) | ❌ | ✅ | ✅ | ✅ | ❌ | <span class="classB">B</span> |
| [JAZZTEL](https://jazztel.es) | ❌ | ❌ | ❌ | ❌ | ❌ | <span class="class0">0</span> |
| [MÁS MOVIL](https://masmovil.es) | ❌ | ❌ | ✅* | ❌ | ❌ | <span class="classD">D</span> |
| [MESFIBRA / MESWIFI](https://meswifi.com) | ❌ | ✅ | ✅ | ✅ | ❌ | <span class="classB">B</span> |
| [MORE DIRECT](https://ca-es.facebook.com/mdplus.informatica) | ✅ | ✅ | ✅ | ✅ | ❌ | <span class="classA">A</span> |
| [MOVISTAR](https://movistar.es) | ❌ | ❌ | ❌ | ❌ | ❌ | <span class="class0">0</span> |
| [ORANGE](https://orange.es) | ❌ | ❌ | ❌ | ❌ | ❌ | <span class="class0">0</span> |
| [PARLEM](https://parlem.com) | ❌ | ❌ | ✅ | ✅ | ❌ | <span class="classC">C</span> |
| [PUNT D'ACCÉS](https://www.puntdacces.coop) | ✅ | ✅ | ✅ | ✅ | ❌ | <span class="classA">A</span> |
| [RACCtel+](https://www.racctelplus.com/) | ❌ | ❌ | ✅ | ❌ | ❌ | <span class="classD">D</span> |
| [SETUP](https://setup.cat) | ✅ | ✅ | ✅ | ✅ | ❌ | <span class="classA">A</span> |
| [SOM CONNEXIÓ](https://somconnexio.coop) | ✅ | ✅ | ✅ | ✅ | ✅ | <span class="classAplus">A+</span> |
| [TICAE](https://www.ticae.com) | ✅ | ✅ | ✅ | ✅ | ❌ | <span class="classA">A</span> |
| [VODAFONE](https://www.vodafone.es) | ❌ | ❌ | ✅ | ❌ | ❌ | <span class="classD">D</span> |
| [XARTIC](https://www.xartic.net) | ✅ | ✅ | ✅ | ✅ | ❌ | <span class="classA">A</span> |
| [XTA](https://www.xta.cat) | ✅ | ✅ | ✅ | ✅ | ❌ | <span class="classA">A</span> |
| [YOIGO](https://www.yoigo.com) | ❌ | ❌ | ❌ | ❌ | ❌ | <span class="class0">0</span> |

** Avaluació de les pàgines web i la possibilitat real d'atenció telefònica en català

\* Només ens van atendre en català una vegada telefònicament. No hi ha apartat en català a la web.
